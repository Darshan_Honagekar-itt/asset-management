using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AssetManagementApi.Models;
using System.Linq;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using AssetManagementApi.Logger;

namespace AssetManagementApi.Controllers
{
    [Authorize(AuthenticationSchemes=JwtBearerDefaults.AuthenticationScheme)]
    [Authorize(Roles="Admin,User")]
    [Route("/api/[controller]")]
    [ApiController]
    public class SoftwareLicenceController : Controller
    {
        private readonly MVCTutorialContext _context = null;
        private ILog _logger;
        public SoftwareLicenceController(MVCTutorialContext context,  ILog logger)
        {
            _context = new MVCTutorialContext();
            _logger = logger;   
        }

        [Authorize(Roles="Admin,User")]
        [HttpGet("Display")]
        public IActionResult Display()
        {
            try
            {
                List<SoftwareLicence> softwareLicenceList =  _context.SoftwareLicences.ToList();
                if(softwareLicenceList.Count!=0)
                {
                    _logger.Information("Exiting Display.");
                    return Ok(softwareLicenceList);
                }
                return NotFound("Software List is Null");
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [HttpPost]
        [Route("Create")]
        public IActionResult Create(SoftwareLicence softwareLicence)
        {
            try
            {
                var newsoftwareLicence = new SoftwareLicence()
                {
                    Id = softwareLicence.Id,
                    Name = softwareLicence.Name,
                    Description = softwareLicence.Description,
                    Manufacturer = softwareLicence.Manufacturer,
                    Price = softwareLicence.Price,
                    Expiry = softwareLicence.Expiry,
                };
                if (_context.SoftwareLicences.Any())
                {
                    SoftwareLicence softwareLicenceCheck = _context.SoftwareLicences.Find(softwareLicence.Id);
                    if(softwareLicenceCheck == null)
                    {
                        _context.SoftwareLicences.Add(newsoftwareLicence);
                        _context.SaveChanges();
                        _logger.Information("Sofware Added succefully!");
                        return Ok("Id" +newsoftwareLicence.Id+ "Added successully");
                    }
                    else
                    {
                        _logger.Error("Can not create a Sofware with same Id");
                        return Conflict($"Id : {newsoftwareLicence.Id} Already exists");
                    }
                }
                else
                {
                    _context.SoftwareLicences.Add(newsoftwareLicence);
                    _context.SaveChanges();
                    _logger.Warning("Database was empty. Added first Sofware succefully");
                    return Ok("Id" +newsoftwareLicence.Id+ "Added successully");
                }
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [HttpDelete]
        [Route("DeleteSoftwareLicence/{id:int}")]   
        public IActionResult DeleteSoftwareLicence(int id)
        {
            try
            {
                SoftwareLicence softwareLicenceDetails = _context.SoftwareLicences.Find(id);
                _logger.Warning($"Sofware {id} will be Deleted");
                if (softwareLicenceDetails != null)
                {
                    _logger.Debug($"Sofware {id} Exists! and Sofware has been fetched from Database");
                    _context.SoftwareLicences.Remove(softwareLicenceDetails);
                    _context.SaveChanges();
                    return Ok($"SoftwareLicence {id} Has been Deleted");
                }
                _logger.Error($"Sofware {id} doesnt exist in the database");
                return NotFound($"Id : {id} doesnt exist");
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [HttpPut]
        [Route("Update")]  
        public IActionResult Update(SoftwareLicence softwareLicence)
        {
            try
            {
                var existingSoftwareLicence = _context.SoftwareLicences.Where(b=>b.Id==softwareLicence.Id).FirstOrDefault<SoftwareLicence>();
                if(existingSoftwareLicence != null)
                {
                    existingSoftwareLicence.Id = softwareLicence.Id;
                    existingSoftwareLicence.Name = softwareLicence.Name;
                    existingSoftwareLicence.Description = softwareLicence.Description;
                    existingSoftwareLicence.Manufacturer = softwareLicence.Manufacturer;
                    existingSoftwareLicence.Expiry = softwareLicence.Expiry;
                    existingSoftwareLicence.Price = softwareLicence.Price;
                    _context.SaveChanges();
                    _logger.Information($"Success! in updating Sofware {softwareLicence.Id} contents");
                    return Ok("Added succesfully");
                }
                _logger.Error($"Can not update Sofware {softwareLicence.Id}, doesn't exist in database");
                return NotFound($"SoftwareLicence {softwareLicence.Id} does not Exist");
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
            
        }

        [Authorize(Roles="Admin,User")]
        [HttpGet("SearchSoftwareLicence/{id:int}")]   
        public IActionResult SearchSoftwareLicence(int id)
        {
           try
           {
                SoftwareLicence softwareLicencedetails = _context.SoftwareLicences.Find(id);
                if (softwareLicencedetails != null)
                {
                    _logger.Information($"Success! in searching Sofware {id}");
                    return Ok(softwareLicencedetails);
                }
                else
                {
                    _logger.Error($"Unable to search Sofware {id}, Not found");
                    return NotFound($"SoftwareLicence Details With Id : {id} doesnt exist");
                }
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }
    } 
}