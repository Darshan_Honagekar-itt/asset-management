using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AssetManagementApi.Models;
using System.Linq;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using AssetManagementApi.Logger;

namespace AssetManagementApi.Controllers
{
    [Authorize(AuthenticationSchemes=JwtBearerDefaults.AuthenticationScheme)]
    [Authorize(Roles="Admin,User")]
    [Route("/api/[controller]")]
    [ApiController]
    public class HardwareController : Controller
    {
        private readonly MVCTutorialContext _context = null;
        
        private ILog _logger;
        public HardwareController(MVCTutorialContext context, ILog logger)
        {
            _context = new MVCTutorialContext();
            _logger = logger;
        }

        [Authorize(Roles="Admin,User")]
        [HttpGet("Display")]
        public IActionResult Display()
        {
            try
            {
                List<Hardware> hardwareList =  _context.Hardwares.ToList();
                if(hardwareList.Count!=0)
                {
                    _logger.Information("Exiting Display.");
                    return Ok(hardwareList);
                }
                return NotFound("Hardware List is null");
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [HttpPost]
        [Route("Create")]
        public IActionResult Create(Hardware hardware)
        {
            try
            {
                var newhardware = new Hardware()
                {
                    Id = hardware.Id,
                    Name = hardware.Name,
                    Supplier = hardware.Supplier,
                    Quantity = hardware.Quantity,
                    Price = hardware.Price,
                };
                Hardware hardwareCheck = _context.Hardwares.Find(hardware.Id);
                if (_context.Hardwares.Any())
                {
                    if(hardwareCheck == null)
                    {
                        _context.Hardwares.Add(newhardware);
                        _context.SaveChanges();
                        _logger.Information("Hardware Added succefully!");
                        return Ok("Id" +newhardware.Id+ "Added successully");
                    }
                    else
                    {
                        _logger.Error("Can not create a Hardware with same Id");
                        return Conflict("Id Already exists");
                    }
                }
                else
                {
                    _context.Hardwares.Add(newhardware);
                    _context.SaveChanges();
                    _logger.Warning("Database was empty. Added first Hardware succefully");
                    return Ok("Id" +newhardware.Id+ "Added successully");  
                }
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [HttpDelete]
        [Route("DeleteHardware/{id:int}")]   
        public IActionResult DeleteHardware(int id)
        {
            
            try
            {
                Hardware hardwareDetails = _context.Hardwares.Find(id);
                _logger.Warning($"Hardware {id} will be Deleted");
                if (hardwareDetails != null)
                {
                    _logger.Debug($"Hardware {id} Exists! and Hardware has been fetched from Database");
                    _context.Hardwares.Remove(hardwareDetails);
                    _context.SaveChanges();
                    return Ok($"Hardware {id} Has been Deleted");
                }
                _logger.Error($"Hardware {id} doesnt exist in the database");
                return NotFound($"Hardware {id} Doesnt exist");
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [HttpPut]
        [Route("Update")]  
        public IActionResult Update(Hardware hardware)
        {
            try
            {
                var existinghardware = _context.Hardwares.Where(b=>b.Id==hardware.Id).FirstOrDefault<Hardware>();
                if(existinghardware != null)
                {
                    existinghardware.Id = hardware.Id;
                    existinghardware.Name = hardware.Name;
                    existinghardware.Price = hardware.Price;
                    existinghardware.Quantity = hardware.Quantity;
                    existinghardware.Supplier = hardware.Supplier;
                    _context.SaveChanges();
                    _logger.Information($"Success! in updating Hardware {hardware.Id} contents");
                    return Ok("Added succesfully");
                }
                else
                {
                    _logger.Error($"Can not update Hardware {hardware.Id}, doesn't exist in database");
                    return BadRequest($"Hardware {hardware.Id} does not Exist");
                }
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }            
        }

        [Authorize(Roles = "Admin  ,User")]
        [HttpGet("SearchHardware/{id:int}")]   
        public IActionResult SearchHardware(int id)
        {
            try
            {
                Hardware hardwaredetails = _context.Hardwares.Find(id);
                if (hardwaredetails != null)
                {
                    _logger.Information($"Success! in searching Hardware {id}");
                    return Ok(hardwaredetails);
                }
                else
                {
                    _logger.Error($"Unable to search Hardware {id}, Not found");
                    return NotFound($"Hardware Details With Id : {id} doesnt exist");
                }
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }
    } 
}