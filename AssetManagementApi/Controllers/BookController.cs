using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AssetManagementApi.Models;
using System.Linq;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using AssetManagementApi.Logger;

namespace AssetManagementApi.Controllers
{
    [Authorize(AuthenticationSchemes=JwtBearerDefaults.AuthenticationScheme)]
    //[Authorize(Roles="Admin,User")]
    [Route("/api/[controller]")]
    [ApiController]
    public class BookController : Controller
    {
        private readonly MVCTutorialContext _context = null;  
        private ILog _logger;  

        public BookController(MVCTutorialContext context, ILog logger)
        {
            _context = new MVCTutorialContext();     
            _logger = logger;     
        }

        [Authorize(Roles="Admin,User")]
        [HttpGet("Display")]
        public IActionResult Display()
        {
            try
            {
                List<Book> bookList =  _context.Books.ToList();
                if(bookList.Count!=0)
                {
                    _logger.Information("Exiting Display.");
                    return Ok(bookList);
                }
                return NotFound("Book List is Null");
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }
        [Authorize(Roles="Admin")]
        [HttpPost]
        [Route("Create")]
        public IActionResult Create(Book book)
        {
            try
            {
                var newbook = new Book()
                {
                    Id = book.Id,
                    Title = book.Title,
                    Author = book.Author,
                    Isbn = book.Isbn,
                    Publisher = book.Publisher,
                    Price = book.Price,
                };
                if (_context.Books.Any())
                {
                    Book bookCheck = _context.Books.Find(book.Id);
                    if (bookCheck == null)
                    {
                        _context.Books.Add(newbook);
                        _context.SaveChanges();
                        _logger.Information("Book Added succefully!");
                        return Ok("Id" + newbook.Id + "Added successully");
                    }
                    else
                    {
                        _logger.Error("Can not create a book with same Id");
                        return Conflict($"Id : {newbook.Id} exist");
                    }
                }
                else
                {
                    _context.Books.Add(newbook);
                    _context.SaveChanges();
                    _logger.Warning("Database was empty. Added first Book succefully");
                    return Ok("Id" +newbook.Id+ "Added successully");
                }
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }
        
        [Authorize(Roles="Admin")]
        [HttpDelete]
        [Route("DeleteBook/{id:int}")]   
        public IActionResult DeleteBook(int id)
        {
            try
            {
                Book bookDetails = _context.Books.Find(id);
                _logger.Warning($"Book {id} will be Deleted");
                if (bookDetails != null)
                {
                    _logger.Debug($"Book {id} Exists! and book has been fetched from Database");
                    _context.Books.Remove(bookDetails);
                    _context.SaveChanges();
                    return Ok($"Book {id} Has been Deleted");
                }
                _logger.Error($"Book {id} doesnt exist in the database");
                return NotFound($"Book {id} Doesnt exist");
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }
        
        [Authorize(Roles="Admin")]
        [HttpPut]
        [Route("Update")]  
        public IActionResult Update(Book book)
        {
            try
            {
                var existingBook = _context.Books.Where(b=>b.Id==book.Id).FirstOrDefault<Book>();
                if(existingBook != null)
                {
                    existingBook.Id = book.Id;
                    existingBook.Title = book.Title;
                    existingBook.Isbn = book.Isbn;
                    existingBook.Author = book.Author;
                    existingBook.Publisher = book.Publisher;
                    existingBook.Price = book.Price;
                    _context.SaveChanges();
                    _logger.Information($"Success! in updating book {book.Id} contents");
                    return Ok("Added succesfully");
                }
                _logger.Error($"Can not update Book {book.Id}, doesn't exist in database");
                return NotFound($"Book {book.Id} does not Exist");
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }

        [Authorize(Roles = "Admin,User")]
        [HttpGet("SearchBook/{id:int}")]   
        public IActionResult SearchBook(int id)
        {
            try
            {
                Book bookdetails = _context.Books.Find(id);
                if (bookdetails != null)
                {
                    _logger.Information($"Success! in searching book {id}");
                    return Ok(bookdetails);
                }
                else
                {
                    _logger.Error($"Unable to search Book {id}, Not found");
                    return NotFound($"Book {id} not Found!");
                }
            }
            catch(Exception e)
            {
                _logger.Error($"Exception!{e.Message}");
                return NotFound(e.Message);
            }
        }
    } 
}