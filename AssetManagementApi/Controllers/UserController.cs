using Microsoft.AspNetCore.Mvc;
using AssetManagementApi.Models;
using System.Linq;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using System.Security.Claims;
using System.Collections.Generic;
using AssetManagementApi.Logger;

namespace AssetManagementApi.Controllers
{
    [Route("/api/[controller]")]
    public class UserController : Controller
    {
        private readonly MVCTutorialContext _context = null;

        private IConfiguration _configuration;
        private ILog _logger;

        public UserController(MVCTutorialContext context, IConfiguration configuration, ILog logger)
        {
            _context = new MVCTutorialContext();
            _configuration = configuration;
            _logger = logger;
        }


        [HttpPost]
        [Route("Signup")]
        public IActionResult Signup([FromBody] UserDataTransferOperation userModel)
        {
            try
            {
                bool isExistingUser = _context.UserAccounts.Any(x => x.Email == userModel.Email);
                if (!isExistingUser)
                {
                    byte[] passwordHash, passwordSalt;

                    CreatePasswordHash(userModel.Password, out passwordHash, out passwordSalt);
                    UserAccount user = new UserAccount()
                    {
                        Email = userModel.Email,
                        Password = passwordHash,
                        Salt = passwordSalt,
                        Role = userModel.UserRole,
                    };
                    _context.UserAccounts.Add(user);
                    _context.SaveChanges();
                    _logger.Information("New User Created");
                    return Ok("User created succefully");
                }
                _logger.Debug("Existing user");
                return Conflict($"Email {userModel.Email} already exists! Login");
            }
            catch (Exception ex)
            {
                _logger.Error($"Exception!{ex.Message}");
                return NotFound(ex.Message);
            }
        }

        [HttpPost("Login")]
        public IActionResult Login([FromBody] UserDataTransferOperation loginDetails)
        {
            try
            {
                if (loginDetails != null)
                {
                    var result = ValidateUser(loginDetails);
                    if (result.exists)
                    {
                        _logger.Information($"Valid user details");
                        var tokenString = GenerateJWT(result.user);
                        _logger.Information("Token Generated");
                        return Ok(new { token = tokenString });
                    }
                    else if (result.user == null)
                    {
                        _logger.Error($"User {loginDetails.Email} Doesn't Exist");
                        return NotFound($"User {loginDetails.Email} Doesn't Exist, check Your email and login again or signup");
                    }
                    else
                    {
                        _logger.Error("Wrong Password");
                        return NotFound($"Wrong password ");
                    }
                }
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.Error($"Exception!{ex.Message}");
                return NotFound(ex.Message);
            }
        }

        [NonAction]
        private (UserAccount user, bool exists) ValidateUser(UserDataTransferOperation loginDetails)
        {
            bool isExistingUser = _context.UserAccounts.Any(x => x.Email == loginDetails.Email);
            if (isExistingUser)
            {
                var existingUser = _context.UserAccounts.FirstOrDefault(x => x.Email == loginDetails.Email);

                if (VerifyPasswordHash(loginDetails.Password, existingUser.Password, existingUser.Salt))
                {
                    return (existingUser, true);
                }
                else
                {
                    return (existingUser, false);
                }
            }
            return (null, false);
        }

        [NonAction]
        private string GenerateJWT(UserAccount userAccount)
        {
            _logger.Debug("Inside token generator");
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var permClaims = new List<Claim>();

            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim(ClaimTypes.Name, userAccount.Email.ToString()));
            permClaims.Add(new Claim(ClaimTypes.Role, userAccount.Role));
            permClaims.Add(new Claim("[Jwt:Key]", "[Jwt:key]"));

            var token = new JwtSecurityToken(
                                _configuration["Jwt:Issuer"],
                                _configuration["Jwt:Audience"],
                                claims: permClaims,
                                expires: DateTime.Now.AddDays(2),
                                signingCredentials: credentials);

            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt_token;
        }

        [NonAction]
        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        [NonAction]
        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            _logger.Debug("inside verifi");
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i]) return false;
                }
                return true;
            }
        }
    }
}