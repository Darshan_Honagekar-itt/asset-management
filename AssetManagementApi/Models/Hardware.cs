﻿using System;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace AssetManagementApi.Models
{
    public partial class Hardware
    {
        [Key]
        [Required(ErrorMessage="The Id is required")]
        public int Id { get; set; }
        
        [Display(Prompt="Enter Name")]
        [Required(ErrorMessage="The Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter a price")]
        [Range(1,int.MaxValue,ErrorMessage="Enter valid Price")]
        [DataType(DataType.Currency)]
        public int? Price { get; set; }
        
        [Display(Prompt="Enter Supplier")]
        [Required(ErrorMessage="The Supplier is required")]
        public string Supplier { get; set; }

        [Display(Prompt="insert an integer")]
        [Required(ErrorMessage="The Quantity is required")]
        public int? Quantity { get; set; }
    }
}
