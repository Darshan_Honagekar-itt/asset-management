﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AssetManagementApi.Models
{
    public partial class UserAccount
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public byte[] Password { get; set; }
        public byte[] Salt { get; set; }
        public string Role { get; set; }
    }
}
