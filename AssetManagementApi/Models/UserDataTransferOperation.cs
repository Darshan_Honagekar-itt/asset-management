using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AssetManagementApi.Models
{
    public class UserDataTransferOperation
    {
        public string Email { get; set; }

        public string Password { get; set; } 

        public string UserRole { get; set; } 
    }
}