using System.ComponentModel.DataAnnotations;

namespace MVC.Models
{
    public class SignupUser
    {
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email id")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        public string Email { get; set; }

        [Required]
        [StringLength(12, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage="Passwords Do not match")]
        public string ConfirmPassword { get; set; }

        public string UserRole { get; set; }
    }
}