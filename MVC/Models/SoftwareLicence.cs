﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace MVC.Models
{
    public partial class SoftwareLicence
    {
        [Key]
        [Required(ErrorMessage="The Id is required")]
        public int Id { get; set; }

        [Display(Prompt="Enter Name")]
        [Required(ErrorMessage="The Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter a price"), Range(1, 1000, ErrorMessage = "Min price: 1, max price: 1000")]
        [DataType(DataType.Currency)]
        public int? Price { get; set; }

        [Display(Prompt="Enter Manufacturer")]
        [Required(ErrorMessage="The Manufacturer is required")]
        public string Manufacturer { get; set; }

        [Display(Prompt="Enter Description")]
        [Required(ErrorMessage="The Description is required")]
        public string Description { get; set; }

        [Display(Prompt="Enter Expiry")]
        [Required(ErrorMessage="The Expiry is required")]
        public DateTime? Expiry { get; set; }
    }
}
