using System.ComponentModel.DataAnnotations;
namespace MVC.Models
{
    public class User
    {
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email id")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}