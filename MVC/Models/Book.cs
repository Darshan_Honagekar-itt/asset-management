﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace MVC.Models
{
    public partial class Book
    {
        [Key]
        [Required(ErrorMessage="The Id is required")]
        public int Id { get; set; }

        [Display(Prompt="Enter Title")]
        [Required]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed")]
        public string Title { get; set; }

        [Display(Prompt="Should Be number")]
        [Range(0,int.MaxValue,ErrorMessage="should be less than 15 digit number")]
        [Required(ErrorMessage = "A numeric value is required")]
        public int? Isbn { get; set; }
        
        [Required(ErrorMessage="The Author name is required"), MaxLength(15, ErrorMessage = "A book author cannot exceed 15 characters")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed")]
        public string Author { get; set; }

        [Required(ErrorMessage = "The Publisher Name is required"), MaxLength(15, ErrorMessage = "A book publisher cannot be empty and should be less than 15 characters")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed")]
        public string Publisher { get; set; }
        
        [Required(ErrorMessage = "Please Enter valid price"), Range(1, 50000, ErrorMessage = "Min price: 1, max price: 50000")]
        public int? Price { get; set; }
    }
}
