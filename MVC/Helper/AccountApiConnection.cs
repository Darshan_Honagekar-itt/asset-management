using System;
using System.Net.Http;
using Microsoft.AspNetCore.Http;

namespace MVC.Helper
{
    public class AccountApiConnection
    {   
        public static HttpClient Initial(HttpContext httpContext)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            var client = new HttpClient(clientHandler);
            client.BaseAddress = new Uri("https://localhost:5004/api/User/");
            return client;
        }  
    }
}