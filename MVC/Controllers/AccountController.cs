using Microsoft.AspNetCore.Mvc;
using MVC.Models;
using System.Net.Http;
using System;
using MVC.Helper;
using System.Threading.Tasks;
using System.Net.Http.Json;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using System.Linq;

namespace MVC.Controllers
{
    [Route("Account")]
    public class AccountController : Controller
    {
        [HttpGet("/Account/Signup")]
        public IActionResult Signup()
        {
            return View();
        }
        [HttpPost("/Account/Signup")]
        public async Task<IActionResult> Signup(SignupUser user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage = await AccountApiConnection.Initial(this.HttpContext).PostAsJsonAsync<SignupUser>("Signup", user);
                    int statusCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else if (statusCode == 409)
                    {
                        TempData["State"] = "Email Exists login with different email";
                        return View();
                    }
                }
                return View();
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        [HttpGet("/Account/Login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost("/Account/Login")]
        public async Task<IActionResult> Login(User user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage = await AccountApiConnection.Initial(this.HttpContext).PostAsJsonAsync<User>("Login", user);
                    string jwtToken = responseMessage.Content.ReadAsStringAsync().Result;

                    int statusCode = Convert.ToInt32(responseMessage.StatusCode);

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        JWT jwt = JsonConvert.DeserializeObject<JWT>(jwtToken);
                        this.HttpContext.Session.SetString("Token", jwt.Token);
                        string Role = DecodeToken(jwt.Token);
                        var claims = new List<Claim>
                        {
                            new Claim(ClaimTypes.Name, user.Email),
                            new Claim(ClaimTypes.Role, Role)
                        };
                        var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                        var principal = new ClaimsPrincipal(identity);
                        var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                        return RedirectToAction("Index", "Home");
                    }
                    else if(statusCode==404)
                    {
                        TempData["State"] = "Invalid Username and Password";
                        return View();
                    }
                }
                ModelState.AddModelError("", "Invalid Username and Password");
                return View();
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("Token");
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }

        [NonAction]
        private string DecodeToken(string token)
        {
            string temp = token;
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadJwtToken(temp);
            var tokenS = jsonToken as JwtSecurityToken;

            var userRole = tokenS.Claims.First(claim => claim.Type == ClaimTypes.Role).Value;

            return userRole;
        }

    }
}