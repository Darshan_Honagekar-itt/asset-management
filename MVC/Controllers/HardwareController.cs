using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MVC.Models;
using System;
using System.Net.Http;
using Newtonsoft.Json;
using MVC.Helper;
using System.Threading.Tasks;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace MVC.Controllers
{
    [Authorize(AuthenticationSchemes=CookieAuthenticationDefaults.AuthenticationScheme)]
    [Authorize(Roles="Admin,User")]
    [Route("Hardware")]
    public class HardwareController : Controller
    {
        [Authorize(Roles="Admin,User")]
        [Route("/Hardware/Display")]
        public async Task<IActionResult> Display()
        {
            try
            {
                List<Hardware> hardwareList = new List<Hardware>();
                HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("Hardware/Display");
                if(responseMessage.IsSuccessStatusCode)
                {
                    var result = responseMessage.Content.ReadAsStringAsync().Result;
                    hardwareList = JsonConvert.DeserializeObject<List<Hardware>>(result);
                    return View(hardwareList);
                }
                return RedirectToAction("ExceptionHandler");
            }
            catch(Exception ex)
            {
                return View(ex.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [Route("/Hardware/Create")]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize(Roles="Admin")]
        [HttpPost("/Hardware/Create")]
        public async Task<IActionResult> Create(Hardware hardware)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage  = await ApiConnection.Initial(this.HttpContext).PostAsJsonAsync<Hardware>("Hardware/Create", hardware);
                    int statusCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Display");
                    }
                    else if(statusCode == 409)
                    {
                        TempData["State"] = "Already Exists create hardware with different id or see all hardwares first";
                        return View();
                    }
                }
                return View();
            }
            catch(Exception e)
            {
                return View(e.Message);
            } 
        }

        [Authorize(Roles="Admin")]
        [Route("/Hardware/Delete/{id:int}")]
        public IActionResult Delete()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("/Hardware/Delete/{id:int}")]   
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).DeleteAsync("Hardware/DeleteHardware/"+id);
                    int statusCode = Convert.ToInt32(responseMessage.StatusCode);
                    if(responseMessage.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Display");
                    }
                    else if(statusCode == 404)
                    {
                        TempData["State"] = $"Hardware {id} Doesn't Exists";
                        return View();
                    }
                }
                return View();
            }
            catch(Exception ex)
            { 
                return View(ex.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [Route("/Hardware/Update/{id:int}")] 
        public async Task<IActionResult> Update(int id)
        {

            HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("Hardware/SearchHardware/"+id);
            int statusCode = Convert.ToInt32(responseMessage.StatusCode);
            if(responseMessage.IsSuccessStatusCode)
            {
                var result = responseMessage.Content.ReadAsStringAsync().Result;
                Hardware hardware = JsonConvert.DeserializeObject<Hardware>(result);
                return View(hardware);
            }
            else if(statusCode == 404)
            {
                TempData["State"] = $"Hardware {id} Doesn't Exists go back and create hardware";
                return View();
            }
            return View();
        }

        [Authorize(Roles="Admin")]
        [HttpPost("/Hardware/Update/{id:int}")] 
        public async Task<IActionResult> Update(Hardware hardware)
        {
            if(ModelState.IsValid)
            {
                HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("Hardware/SearchHardware/"+hardware.Id);
                int statusCode = Convert.ToInt32(responseMessage.StatusCode);
                if(responseMessage.IsSuccessStatusCode && statusCode!=404)
                {
                    HttpResponseMessage response = await ApiConnection.Initial(this.HttpContext).PutAsJsonAsync("Hardware/Update", hardware);
                    return RedirectToAction("Display");
                }
                else if(statusCode == 404)
                {
                    TempData["State"] = "Doesn't Exists go back and create hardware with different id or see all hardwares first";
                    RedirectToAction("ErrorHandler",new {hardware.Id});
                }
            }
            return View();
        }
        
        [Authorize(Roles="Admin,User")]
        [Route("/Hardware/Search")]
        public IActionResult Search()
        {
            return View();
        }

        [Authorize(Roles="Admin,User")]
        [HttpPost("/Hardware/SearchHardware")]   
        public async Task<IActionResult> SearchHardware(int id)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("Hardware/SearchHardware/"+id);
                    if(responseMessage.IsSuccessStatusCode)
                    {
                        var result = responseMessage.Content.ReadAsStringAsync().Result;
                        Hardware hardware = JsonConvert.DeserializeObject<Hardware>(result);
                        return View(hardware);
                    }
                    else
                    {
                        TempData["State"] = $"Hardware {id} doesn't exists click on create to add new hardware";
                        return View("Search");
                    }
                }    
                return View("Search");
            }
            catch(Exception ex)
            {
                return View(ex.Message);
            }
        }
    } 
}