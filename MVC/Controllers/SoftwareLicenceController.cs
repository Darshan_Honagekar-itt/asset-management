using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MVC.Models;
using System;
using System.Net.Http;
using Newtonsoft.Json;
using MVC.Helper;
using System.Threading.Tasks;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace MVC.Controllers
{
    [Authorize(AuthenticationSchemes=CookieAuthenticationDefaults.AuthenticationScheme)]
    [Authorize(Roles="Admin,User")]
    [Route("SoftwareLicence")]
    public class SoftwareLicenceController : Controller
    {
        [Authorize(Roles="Admin,User")]
        [Route("/SoftwareLicence/Display")]
        public async Task<IActionResult> Display()
        {
            try
            {
                List<SoftwareLicence> softwareLicenceList = new List<SoftwareLicence>();
                HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("SoftwareLicence/Display");
                if(responseMessage.IsSuccessStatusCode)
                {
                    var result = responseMessage.Content.ReadAsStringAsync().Result;
                    softwareLicenceList = JsonConvert.DeserializeObject<List<SoftwareLicence>>(result);
                    return View(softwareLicenceList);
                }
                return RedirectToAction("ExceptionHandler");
            }
            catch(Exception ex)
            {
                return View(ex.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [Route("/SoftwareLicence/Create")]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize(Roles="Admin")]
        [HttpPost("/SoftwareLicence/Create")]
        public async Task<IActionResult> Create(SoftwareLicence softwareLicence)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage  = await ApiConnection.Initial(this.HttpContext).PostAsJsonAsync<SoftwareLicence>("SoftwareLicence/Create", softwareLicence);
                    int statusCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Display");
                    }
                    else if(statusCode == 409)
                    {
                        TempData["State"] = "Already Exists create softwareLicence with different id or see all softwareLicences first";
                        return View();
                    }
                }
                return View();
            }
            catch(Exception e)
            {
                return View(e.Message);
            } 
        }

        [Authorize(Roles="Admin")]
        [Route("/SoftwareLicence/Delete/{id:int}")]
        public IActionResult Delete()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("/SoftwareLicence/Delete/{id:int}")]   
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).DeleteAsync("SoftwareLicence/DeleteSoftwareLicence/"+id);
                    int statusCode = Convert.ToInt32(responseMessage.StatusCode);
                    if(responseMessage.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Display");
                    }
                    else if(statusCode == 404)
                    {
                        TempData["State"] = $"SoftwareLicence {id} Doesn't Exists";
                        return View();
                    }
                }
                return View();
            }
            catch(Exception ex)
            { 
                return View(ex.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [Route("/SoftwareLicence/Update/{id:int}")] 
        public async Task<IActionResult> Update(int id)
        {

            HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("SoftwareLicence/SearchSoftwareLicence/"+id);
            int statusCode = Convert.ToInt32(responseMessage.StatusCode);
            if(responseMessage.IsSuccessStatusCode)
            {
                var result = responseMessage.Content.ReadAsStringAsync().Result;
                SoftwareLicence softwareLicence = JsonConvert.DeserializeObject<SoftwareLicence>(result);
                return View(softwareLicence);
            }
            else if(statusCode == 404)
            {
                TempData["State"] = $"SoftwareLicence {id} Doesn't Exists go back and create softwareLicence";
                return View();
            }
            return View();
        }

        [Authorize(Roles="Admin")]
        [HttpPost("/SoftwareLicence/Update/{id:int}")] 
        public async Task<IActionResult> Update(SoftwareLicence softwareLicence)
        {
            if(ModelState.IsValid)
            {
                HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("SoftwareLicence/SearchSoftwareLicence/"+softwareLicence.Id);
                int statusCode = Convert.ToInt32(responseMessage.StatusCode);
                if(responseMessage.IsSuccessStatusCode && statusCode!=404)
                {
                    HttpResponseMessage response = await ApiConnection.Initial(this.HttpContext).PutAsJsonAsync("SoftwareLicence/Update", softwareLicence);
                    return RedirectToAction("Display");
                }
                else if(statusCode == 404)
                {
                    TempData["State"] = "Doesn't Exists go back and create softwareLicence with different id or see all softwareLicences first";
                    RedirectToAction("ErrorHandler",new {softwareLicence.Id});
                }
            }
            return View();
        }
        
        [Authorize(Roles="Admin,User")]
        [Route("/SoftwareLicence/Search")]
        public IActionResult Search()
        {
            return View();
        }

        [Authorize(Roles="Admin,User")]
        [HttpPost("/SoftwareLicence/SearchSoftware")]   
        public async Task<IActionResult> SearchSoftware(int id)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("SoftwareLicence/SearchSoftwareLicence/"+id);
                    if(responseMessage.IsSuccessStatusCode)
                    {
                        var result = responseMessage.Content.ReadAsStringAsync().Result;
                        SoftwareLicence softwareLicence = JsonConvert.DeserializeObject<SoftwareLicence>(result);
                        return View(softwareLicence);
                    }
                    else
                    {
                        TempData["State"] = $"SoftwareLicence {id} doesn't exists click on create to add new softwareLicence";
                        return View("Search");
                    }
                }    
                return View("Search");
            }
            catch(Exception ex)
            {
                return View(ex.Message);
            }
        }
    } 
}