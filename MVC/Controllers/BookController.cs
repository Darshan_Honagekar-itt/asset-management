using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MVC.Models;
using System;
using System.Net.Http;
using Newtonsoft.Json;
using MVC.Helper;
using System.Threading.Tasks;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace MVC.Controllers
{
    [Authorize(AuthenticationSchemes=CookieAuthenticationDefaults.AuthenticationScheme)]
    //[Authorize(Roles="Admin,User")]
    [Route("Book")]
    public class BookController : Controller
    {
        [Authorize(Roles="Admin,User")]
        [Route("/Book/Display")]
        public async Task<IActionResult> Display()
        {
            try
            {
                List<Book> bookList = new List<Book>();
                HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("Book/Display");
                if(responseMessage.IsSuccessStatusCode)
                {
                    var result = responseMessage.Content.ReadAsStringAsync().Result;
                    bookList = JsonConvert.DeserializeObject<List<Book>>(result);
                    return View(bookList);
                }
                return RedirectToAction("ExceptionHandler");
            }
            catch(Exception ex)
            {
                return View(ex.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [Route("/Book/Create")]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize(Roles="Admin")]
        [HttpPost("/Book/Create")]
        public async Task<IActionResult> Create(Book book)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage  = await ApiConnection.Initial(this.HttpContext).PostAsJsonAsync<Book>("Book/Create", book);
                    int statusCode = Convert.ToInt32(responseMessage.StatusCode);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Display");
                    }
                    else if(statusCode == 409)
                    {
                        TempData["State"] = "Already Exists create book with different id or see all books first";
                        return View();
                    }
                }
                return View();
            }
            catch(Exception e)
            {
                return View(e.Message);
            } 
        }

        [Authorize(Roles="Admin")]
        [Route("/Book/Delete/{id:int}")]
        public IActionResult Delete()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("/Book/Delete/{id:int}")]   
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).DeleteAsync("Book/DeleteBook/"+id);
                    int statusCode = Convert.ToInt32(responseMessage.StatusCode);
                    if(responseMessage.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Display");
                    }
                    else if(statusCode == 404)
                    {
                        TempData["State"] = $"Book {id} Doesn't Exists";
                        return View();
                    }
                }
                return View();
            }
            catch(Exception ex)
            { 
                return View(ex.Message);
            }
        }

        [Authorize(Roles="Admin")]
        [Route("/Book/Update/{id:int}")] 
        public async Task<IActionResult> Update(int id)
        {

            HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("Book/SearchBook/"+id);
            int statusCode = Convert.ToInt32(responseMessage.StatusCode);
            if(responseMessage.IsSuccessStatusCode)
            {
                var result = responseMessage.Content.ReadAsStringAsync().Result;
                Book book = JsonConvert.DeserializeObject<Book>(result);
                return View(book);
            }
            else if(statusCode == 404)
            {
                TempData["State"] = $"Book {id} Doesn't Exists go back and create book";
                return View();
            }
            return View();
        }

        [Authorize(Roles="Admin")]
        [HttpPost("/Book/Update/{id:int}")] 
        public async Task<IActionResult> Update(Book book)
        {
            if(ModelState.IsValid)
            {
                HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("Book/SearchBook/"+book.Id);
                int statusCode = Convert.ToInt32(responseMessage.StatusCode);
                if(responseMessage.IsSuccessStatusCode && statusCode!=404)
                {
                    HttpResponseMessage response = await ApiConnection.Initial(this.HttpContext).PutAsJsonAsync("Book/Update", book);
                    return RedirectToAction("Display");
                }
                else if(statusCode == 404)
                {
                    TempData["State"] = "Doesn't Exists go back and create book with different id or see all books first";
                    RedirectToAction("ErrorHandler",new {book.Id});
                }
            }
            return View();
        }
        
        [Authorize(Roles="Admin,User")]
        [Route("/Book/Search")]
        public IActionResult Search()
        {
            return View();
        }

        [Authorize(Roles="Admin,User")]
        [HttpPost("/Book/SearchBook")]   
        public async Task<IActionResult> SearchBook(int id)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    HttpResponseMessage responseMessage = await ApiConnection.Initial(this.HttpContext).GetAsync("Book/SearchBook/"+id);
                    if(responseMessage.IsSuccessStatusCode)
                    {
                        var result = responseMessage.Content.ReadAsStringAsync().Result;
                        Book book = JsonConvert.DeserializeObject<Book>(result);
                        return View(book);
                    }
                    else
                    {
                        TempData["State"] = $"Book {id} doesn't exists click on create to add new book";
                        return View("Search");
                    }
                }    
                return View("Search");
            }
            catch(Exception ex)
            {
                return View(ex.Message);
            }
        }
    } 
}